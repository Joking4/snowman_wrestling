﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameUtils
{
    public interface Menu
    {
        void controllerUpdate();
        void draw(GameTime gameTime);
        Boolean isOpen();
    }
}
