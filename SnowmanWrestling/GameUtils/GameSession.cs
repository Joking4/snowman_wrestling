﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameUtils
{
    public class GameSession
    {
        //reference to the active game object
        Game game { get; set; }
        //contains player data so that it can be shared between levels
        Player[] players { get; set; }
    }
}
