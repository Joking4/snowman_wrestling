﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameUtils
{
    public interface Level
    {
        void initialize(GameSession gameSession);
        void loadContent(GraphicsDevice graphicsDevice);
        void onContentLoaded();
        void unloadContent();
        void controllerUpdate();
        void collisionDetection();
        void draw(GameTime gameTime);
    }
}
